require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /show" do
    let(:test_taxon) { create(:taxon) }

    before do
      get potepan_category_path(test_taxon.id)
    end

    it "httpステータスが200を返すこと" do
      expect(response).to have_http_status "200"
    end

    it "意図したページへrenderできているか" do
      expect(response).to render_template(:show)
    end
  end
end
