require 'rails_helper'

RSpec.describe "Spree::Productモデル内のメソッドのテスト", type: :model do
  let(:test_taxon) { create(:taxon) }
  let(:test_product) { create(:product, taxons: [test_taxon]) }
  let(:test_product_without_taxon) { create(:product, name: "#{test_product.name}_without_taxon") }
  let(:products) { create_list(:product, 5, taxons: [test_product.taxons.first]) }

  describe "related_productsのテスト" do
    context "productがtaxon情報を持つ時" do
      it "同じtaxon情報を持つ他のproductを返すこと" do
        expect(test_product.related_products).to eq [products[0], products[1], products[2], products[3], products[4]]
      end
    end

    context "productがtaxon情報を持たない時" do
      it "related_productが空配列[]を返すこと" do
        expect(test_product_without_taxon.related_products).to eq []
      end
    end
  end
end
