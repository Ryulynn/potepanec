require 'rails_helper'

RSpec.feature "products/show.html.erbに正しい情報の表示", type: :feature do
  given(:test_image_0) { create(:image) }
  given(:test_image_1) { create(:image) }
  given(:test_variant) { create(:variant, is_master: true, images: [test_image_0, test_image_1]) }
  given(:test_taxon) { create(:taxon) }
  given(:test_product) { create(:product, master: test_variant, taxons: [test_taxon]) }
  given(:test_product_id) { test_product.id }
  given(:test_product_without_taxon) { create(:product, name: "#{test_product.name}_without_taxon") }
  given(:test_related_image) { create(:image) }
  given(:test_related_variant) { create(:variant, is_master: true, images: [test_related_image]) }
  given!(:test_related_product) { create(:product, master: test_related_variant, taxons: [test_product.taxons.first], name: "#{test_product.name}_related") }

  background do
    visit potepan_product_path(test_product_id)
  end

  scenario "商品タイトル部分のHomeをクリックしたときに/potepanにアクセスすること" do
    within("div.main-wrapper") do
      click_link 'Home'
    end
    expect(current_path).to eq potepan_path
  end

  feature "一覧ページへ戻るをクリックしたときに正しいページにアクセスすること" do
    context "productがtaxonを持つとき" do
      scenario "一覧ページへ戻るをクリックしたときに正しいcategoriesのshowページにアクセスすること" do
        click_link '一覧ページへ戻る'
        expect(current_path).to eq potepan_category_path(test_taxon.id)
      end
    end

    context "productがtaxonを持たないとき" do
      let(:test_product_id) { test_product_without_taxon.id }

      scenario "一覧ページへ戻るをクリックしたときにHomeへアクセスすること" do
        click_link '一覧ページへ戻る'
        expect(current_path).to eq potepan_path
      end
    end
  end

  feature "データに合わせた商品の情報が表示されること" do
    scenario "商品名が正しいこと" do
      expect(page).to have_selector '#product_name', text: test_product.name
    end

    scenario "値段が正しいこと" do
      expect(page).to have_selector '#product_price', text: test_product.display_price
    end

    scenario "商品説明が正しいこと" do
      expect(page).to have_selector '#product_description', text: test_product.description
    end

    feature "商品画像が正しいこと" do
      feature "大きい画像の表示" do
        scenario "image_large_0が正しく表示できていること" do
          expect(page).to have_selector "#image_large_0 > img[src*='/spree/products/#{test_product.images.first.id}/large/#{test_product.images.first.attachment_file_name}']"
        end

        scenario "image_large_1が正しく表示できていること" do
          expect(page).to have_selector "#image_large_1 > img[src*='/spree/products/#{test_product.images.second.id}/large/#{test_product.images.second.attachment_file_name}']"
        end
      end

      feature "小さい画像の表示" do
        scenario "image_small_0が正しく表示できていること" do
          expect(page).to have_selector "#image_small_0 > img[src*='/spree/products/#{test_product.images.first.id}/small/#{test_product.images.first.attachment_file_name}']"
        end

        scenario "image_small_1が正しく表示できていること" do
          expect(page).to have_selector "#image_small_1 > img[src*='/spree/products/#{test_product.images.second.id}/small/#{test_product.images.second.attachment_file_name}']"
        end
      end
    end
  end

  feature "関連商品の情報が正しく表示されること" do
    scenario "関連商品の商品名が正しく表示されていること" do
      expect(page).to have_selector "#related_product_name_0", text: test_related_product.name
    end

    scenario "関連商品の値段が正しく表示されていること" do
      expect(page).to have_selector "#related_product_price_0", text: test_related_product.display_price
    end

    scenario "関連商品の画像が正しく表示されていること" do
      expect(page).to have_selector "#related_product_image_0 > img[src*='/spree/products/#{test_related_product.images.first.id}/large/#{test_related_product.images.first.attachment_file_name}']"
    end

    scenario "関連商品をクリックした時に、対象の商品詳細ページにアクセスすること" do
      click_link test_related_product.name
      expect(current_path).to eq potepan_product_path(test_related_product.id)
    end
  end
end
