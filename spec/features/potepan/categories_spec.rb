require 'rails_helper'

RSpec.feature "categories/show.html.erbに正しい情報の表示", type: :feature do
  given(:test_image) { create(:image) }
  given(:test_variant) { create(:variant, is_master: true, images: [test_image]) }
  given(:test_taxonomy) { create(:taxonomy) }
  given(:test_taxon) { create(:taxon, taxonomy_id: test_taxonomy.id) }
  given!(:test_product) { create(:product, master: test_variant, taxons: [test_taxon]) }
  given(:test_product_only) { create(:product, name: "#{test_product.name}_only") } # taxon情報を持たないproductの作成

  background do
    visit potepan_category_path(test_taxon.id)
  end

  feature "サイドバーのカテゴリー分類が正しくできているか" do
    scenario "大分類(taxonomy)が正しく表示できていること" do
      expect(page).to have_selector '#side_nav_root_category_0', text: test_taxonomy.name
    end

    scenario "小分類(taxonの最下層)が正しく表示できていること" do
      expect(page).to have_selector '#side_nav_leaf_category_1_root_0', text: test_taxon.name
    end

    scenario "小分類の登録数が正しく表示できていること" do
      expect(page).to have_selector '#side_nav_leaf_category_1_root_0', text: test_taxon.all_products.count
    end
  end

  feature "商品情報が正しく表示できているか" do
    scenario "商品画像が正しく表示できていること" do
      expect(page).to have_selector "#product_image_0 > img[src*='/spree/products/#{test_product.images.first.id}/large/#{test_product.images.first.attachment_file_name}']"
    end

    scenario "商品名が正しく表示できていること" do
      expect(page).to have_selector "#product_name_0", text: test_product.name
    end

    scenario "値段が正しく表示できていること" do
      expect(page).to have_selector "#product_price_0", text: test_product.display_price
    end
  end

  scenario "サイドバーの商品数と表示している商品数が同じこと" do
    expect(all(".productBox").size).to eq test_taxon.all_products.count
  end

  scenario "taxon情報を持たないproductが表示されないこと" do
    within("div.productBox") do
      expect(page).not_to have_content test_product_only.name
    end
  end

  scenario "商品クリック時に正しい商品詳細ページに遷移できるか" do
    click_link test_product.name
    expect(current_path).to eq potepan_product_path(test_product.id)
  end
end
