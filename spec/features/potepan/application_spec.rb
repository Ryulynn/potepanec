require 'rails_helper'

RSpec.feature "application.html.erbに正しい情報の表示", type: :feature do
  given(:test_taxon) { create(:taxon) }
  given(:test_product) { create(:product, taxons: [test_taxon]) }

  feature "/potepanに遷移したときのテスト" do
    scenario "遷移先に合わせたタイトルが表示されること" do
      visit potepan_path
      expect(title).to eq "BIGBAG Store"
    end
  end

  feature "products#showに遷移したときのテスト" do
    background do
      visit potepan_product_path(test_product.id)
    end

    scenario "ヘッダー部分のHomeをクリックしたときに/potepanにアクセスすること" do
      within("header") do
        click_link 'Home'
      end
      expect(current_path).to eq potepan_path
    end

    scenario "ホームページロゴをクリックしたときのホームページにアクセスすること(header)" do
      find('#home_logo').click
      expect(current_path).to eq potepan_path
    end

    scenario "遷移先の商品詳細ページに合わせたタイトルが表示されること" do
      expect(title).to eq "#{test_product.name} - BIGBAG Store"
    end
  end
end
