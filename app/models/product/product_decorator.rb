module ProductDecorator
  def related_products
    Spree::Product.
      joins(:taxons).
      includes(master: [:images, :default_price]).
      where(spree_products_taxons: { taxon_id: taxons }).
      where.not(id: id).
      distinct.
      order(:id)
  end

  Spree::Product.prepend self
end
